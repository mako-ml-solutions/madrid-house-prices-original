# Madrid house prices original

## Datasets

* The original dataset. More information about the dataset: https://www.kaggle.com/mirbektoktogaraev/madrid-real-estate-market

* The clean dataset with only the necessary columns to develop a neural network.

## Files

* EDA: analyzing, cleaning and selecting relevant columns of the original dataset.

* Coordinates: an example of how to turn addresses into their latitude and longitude coordinates.

* Keras model: final steps to transform the data into tensors, creating a neural network, training it and validating the results.

## Steps

- `git clone https://gitlab.com/mako-ml-solutions/madrid-house-prices-original.git`
- `cd madrid-house-prices-original `
- `jupyter notebook`

## Developed with

* [Pandas](https://pandas.pydata.org/pandas-docs/stable/index.html)
* [Seaborn](https://seaborn.pydata.org/)
* [GeoPy](https://geopy.readthedocs.io/en/stable/)
* [Keras](https://www.tensorflow.org/guide/keras)
* [Tensorflow](https://www.tensorflow.org/)

## License

The project is licensed under the GPL-3.0-or-later. See the [COPYING.md](COPYING.md) file for details.
